#!usr/bin/env bash

echo Installing LAMP Stack with PHPMYADMIN [APACHE, MARIA-DB, PHP]; sleep 2

echo Updating system and installing essentials dependencies..; sleep 2

sudo apt update

sudo apt-get install software-properties-common dirmngr apt-transport-https -y

sudo apt-get install zip unzip -y

sudo apt install curl -y




echo Adding PPA; sleep 2

sudo add-apt-repository ppa:ondrej/php -y

sudo add-apt-repository ppa:ondrej/apache2 -y



echo Installing Apache Server; sleep 2

sudo apt update

sudo apt install apache2 -y


echo Installing UFW Firewall...; sleep 2

sudo apt install ufw -y

echo Select y and press enter; sleep 1

sudo ufw enable

sudo ufw allow 22/tcp

sudo ufw allow 80/tcp

sudo ufw allow 443/tcp



sudo apt-get update

echo Installing Maria-DB; sleep 2

sudo apt install mariadb-server mariadb-client -y



echo Installing PHP 8; sleep 2

sudo apt install php8.0 php8.0-cli php8.0-common php8.0-curl php8.0-gd php8.0-intl php8.0-mbstring php8.0-mysql php8.0-imagick php8.0-opcache php8.0-readline php8.0-xml php8.0-xsl php8.0-zip php8.0-bz2 php8.0-bcmath libapache2-mod-php8.0 -y




echo Ignore the 1st password promt, then press Y, then set new password, select Y for rest of the prompts; sleep 2

sudo mysql_secure_installation




echo Installing PHPMYADMIN manually; sleep 2

sudo mv phpmyadmin.conf ~

sudo mv config.inc.php ~

#sudo mv apache2-utils.ufw.profile ~






echo Downloading PHPMYADMIN; sleep 2

sudo wget -P /usr/share https://www.phpmyadmin.net/downloads/phpMyAdmin-latest-all-languages.zip

sudo unzip /usr/share/*phpMyAdmin*.zip -d /usr/share

sudo mv /usr/share/phpMyAdmin-*-all-languages /usr/share/phpmyadmin

sudo rm /usr/share/phpmyadmin.zip




sudo adduser $USER www-data

sudo chown -R www-data:www-data /usr/share/phpmyadmin







echo Copying Configuration Files; sleep 2


sudo mv ~/phpmyadmin.conf /etc/apache2/conf-available/

sudo a2enconf phpmyadmin

systemctl reload apache2

sudo rm /usr/share/phpmyadmin/config.sample.inc.php






#sudo mv apache2-utils.ufw.profile /etc/ufw/applications.d

sudo mv ~/config.inc.php /usr/share/phpmyadmin

sudo chmod 644 /usr/share/phpmyadmin/config.inc.php

sudo mkdir /usr/share/phpmyadmin/tmp

sudo chown -R www-data:www-data /usr/share/phpmyadmin/tmp

sudo ln -s /usr/share/phpmyadmin /var/www/html

sudo systemctl reload apache2






echo Enter MARIA-DB password for fixing login and database issue- may ask for 3 times simultaneously; sleep 2

mysql -u root -p << EOF

UPDATE mysql.user SET plugin = 'mysql_native_password' WHERE user = 'root' AND plugin = 'unix_socket';

FLUSH PRIVILEGES;

exit

EOF

sudo systemctl reload apache2





cd /usr/share/phpmyadmin/sql

mysql -u root -p < create_tables.sql 

mysql -u root -p -e 'GRANT SELECT, INSERT, DELETE, UPDATE ON phpmyadmin.* TO 'pma'@'localhost' IDENTIFIED BY "pmapassword"'

sudo systemctl reload apache2




echo Installation completed!!!; sleep 1

exit
