#!usr/bin/env bash


sudo rm /var/www/html/index.html


echo Installing WordPress; sleep 2


sudo wget -P /var/www/html https://wordpress.org/latest.tar.gz


sudo tar -zvxf /var/www/html/latest.tar.gz -C /var/www/html

#shopt -s dotglob

sudo mv /var/www/html/wordpress/* /var/www/html

sudo rm /var/www/html/latest.tar.gz

sudo rmdir /var/www/html/wordpress



sudo find /var/www/html -type d -exec chmod 750 {} \;

sudo find /var/www/html -type f -exec chmod 640 {} \;

sudo systemctl restart apache2

echo Done....; sleep 1

sudo adduser $USER www-data

sudo chown -R www-data:www-data /var/www/html

sudo chmod u=rwX,g=srX,o=rX -R /var/www/html

exit
